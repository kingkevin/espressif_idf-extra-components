**WARNING**:
This repository is outdated and not required anymore.
This was a fork of [esp_tinyusb v1.0.2](https://components.espressif.com/components/espressif/esp_tinyusb/versions/1.0.2) and added the missing USB DFU runtime support, using tinyUSB v0.14.1.
[esp_tinyusb v1.1.0](https://components.espressif.com/components/espressif/esp_tinyusb/versions/1.1.0) [updated](https://github.com/espressif/esp-usb/commit/b74ee15a2ecd3dee19f7fb27a4a7bdeb22bcbc68) tinyUSB to v0.14.2 and added DFU support.

# Espressif IDF Extra Components

This repository aims to store [ESP-IDF](https://github.com/espressif/esp-idf) extra components 
which have been seperated and uploaded into [IDF Component Manager](https://components.espressif.com/).

## Important note
This repository is used only for Espressif components, it is not mean to store third party components.

## Resources

[IDF Component Manager](https://github.com/espressif/idf-component-manager) repository.

## Contribution

We welcome all contributions to the Component Manager project.

You can contribute by fixing bugs, adding features, adding documentation or reporting an [issue](https://github.com/espressif/idf-extra-components/issues). We accept contributions via [Github Pull Requests](https://docs.github.com/en/pull-requests/collaborating-with-pull-requests/proposing-changes-to-your-work-with-pull-requests/about-pull-requests).

Before reporting an issue, make sure you've searched for a similar one that was already created.
